# Sistemas Electrónicos Digitales - Trabajo FPGA Digilet NEXYS 4 DDR
### Desarrollado por Alejandro López Guerrero
### Enlaces de interés
- [Edaplayground](https://edaplayground.com/) - Entorno de puebas
- [Nexys 4 DDR](https://reference.digilentinc.com/reference/programmable-logic/nexys-4-ddr/start?redirect=1) - Digilent
