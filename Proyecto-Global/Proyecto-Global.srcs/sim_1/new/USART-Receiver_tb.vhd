library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity UART_tb is
end UART_tb;


architecture test of UART_tb is

COMPONENT UART_RX
    PORT(
          i_Clk       : in  std_logic;
          i_RX_Serial : in  std_logic;
          o_RX_DV     : out std_logic;
          o_RX_Byte   : out std_logic_vector(7 downto 0)
        ); 
    END COMPONENT;
    
   signal i_CLK : std_logic := '0';
   signal i_RX_Serial : std_logic := '0';
   signal o_RX_Byte: std_logic_vector(7 downto 0);
   
   constant clk_period : time := 20 ns;
   constant clk_period_uart : time := 1000 ns;
begin
 uut: UART_RX PORT MAP (
          i_CLK=> i_CLK,
          i_RX_Serial => i_RX_Serial,
          o_RX_Byte=>o_RX_Byte
        );
CLK_TEST:process
  begin
  	i_CLK <= '1';
  	wait for clk_period/2;
  	i_CLK <= '0';
  	wait for clk_period/2;
  end process;
  
--RESET<= '1', '0' after 0.25*clk_period;
SIN_TEST:process
  begin

    i_RX_Serial<='1';
    wait for 9*clk_period_uart;
    -------------------------
    i_RX_Serial<='0';
    wait for 2*clk_period_uart;
    i_RX_Serial<='1';
    wait for 4*clk_period_uart;
    i_RX_Serial<='0';
    wait for 1*clk_period_uart;
    i_RX_Serial<='1';
    wait for 12*clk_period_uart;
    -------------------------
    i_RX_Serial<='0';
    wait for 1*clk_period_uart;
    i_RX_Serial<='1';
    wait for 1*clk_period_uart;
    i_RX_Serial<='0';
    wait for 4*clk_period_uart;
    i_RX_Serial<='1';
    wait for 12*clk_period_uart;
    -------------------------

    i_RX_Serial<='0';
    wait for 2*clk_period_uart;
    i_RX_Serial<='1';
    wait for 4*clk_period_uart;
    i_RX_Serial<='0';
    wait for 1*clk_period_uart;
    i_RX_Serial<='1';
    wait for 8*clk_period_uart;
    
  end process;
  
  process
    begin
        wait for 50*clk_period_uart;
        assert false
            report "[EXITO] >> Simulacion finalizada <<"
                severity failure;
    end process;

end test;