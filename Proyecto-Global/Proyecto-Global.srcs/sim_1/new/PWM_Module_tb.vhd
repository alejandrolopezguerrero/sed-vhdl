library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;

entity PWM_Module_tb is
end PWM_Module_tb;

architecture test of PWM_Module_tb is 

  component PWM_Module
    port(
      reset  : in  std_logic;
      clk    : in  std_logic;
      duty   : in  std_logic_vector(7 downto 0);
      enable : in  std_logic;
      pwm_out: out std_logic
    );
  end component;

  --Inputs
  signal reset  : std_logic;
  signal clk    : std_logic;
  signal duty   : std_logic_vector(7 downto 0);
  signal enable : std_logic;

  --Outputs
  signal pwm_out: std_logic;

   -- Clock period definitions
   constant clk_period : time := 10 ns;
   constant duty_value : positive := 200;

begin

  -- Instantiate the Unit Under Test (UUT)
  uut: PWM_Module
    port map (
      reset   => reset,
      clk     => clk,
      duty    => duty,
      enable  => enable,
      pwm_out    => pwm_out
    );


  clk_process :process
  begin
    clk <= '0';
    wait for 0.5 * clk_period;
    clk <= '1';
    wait for 0.5 * clk_period;
  end process;

  reset  <= '0' after 0.25 * clk_period, '1' after 0.75 * clk_period;
  
  stim_proc: process
  begin		
    --duty <= std_logic_vector(to_unsigned(duty_value, duty'length));
    duty <="11110111";
    enable   <= '1';

    wait until reset = '1';

    wait until clk = '1';
    for i in 1 to 2**(duty'length + 1) loop
      wait until clk = '1';
    end loop;

    wait until clk = '1';
    wait until clk = '1';
    wait until clk = '0';
    enable   <= '0';
    wait until clk = '1';
    wait until clk = '1';
    wait until clk = '0';
    enable   <= '1';
    wait until clk = '1';
    for i in 1 to 2**duty'length loop
      wait until clk = '1';
    end loop;

    wait for 0.5 * clk_period;

    assert false
      report "[SUCCESS]: simulation finished."
      severity failure;
  end process;

end test;