library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity TOP_MODULE_tb is
end TOP_MODULE_tb;


architecture test of TOP_MODULE_tb is

    COMPONENT TOP_MODULE
    Port (
        CLK: IN STD_LOGIC; --Main Clock
        RESET: IN STD_LOGIC;-- Active on low level
        UART_IN: IN STD_LOGIC;-- USART Rx
        SOUND_PWM: OUT STD_LOGIC; --Output Sound
        SOUND_SD: OUT STD_LOGIC
    );
    END COMPONENT;
    
   signal CLK : std_logic := '0';
   signal CLK_UART : std_logic := '0';
   signal RESET : std_logic := '0';
   signal UART_IN : std_logic := '0';
   signal SOUND_PWM: std_logic;
   signal SOUND_SD: std_logic:='1';
   
   constant clk_period : time := 10 ns;
   constant clk_uart_period : time := 20 ns;
begin
 uut: TOP_MODULE PORT MAP (
          CLK=>CLK,
          RESET => RESET,
          UART_IN => UART_IN,
          SOUND_PWM=>SOUND_PWM
        );
    CLK_TEST:process
    begin
  	    CLK <= '0';
  	    wait for clk_period/2;
  	    CLK <= '1';
        wait for clk_period/2;
    end process;
  
    CLK_USART_TEST:process
    begin
  	    CLK_UART <= '1';
  	    wait for clk_uart_period/2;
  	    CLK_UART <= '0';
        wait for clk_uart_period/2;
    end process;
  
RESET<= '0', '1' after 0.25*clk_period;
SOUND_SD<='1';
USART_TEST:process
  begin
    
    UART_IN<='1';
    wait for 8*clk_uart_period;
    -------------------------
    UART_IN<='0';
    wait for 6*clk_uart_period;
    UART_IN<='1';
    wait for 1*clk_uart_period;
    UART_IN<='0';
    wait for 1*clk_uart_period;
    UART_IN<='1';
    wait for 2000*clk_uart_period;
    -------------------------

    UART_IN<='0';
    wait for 1*clk_uart_period;
    UART_IN<='1';
    wait for 6*clk_uart_period;
    UART_IN<='0';
    wait for 1*clk_uart_period;
    UART_IN<='1';
    wait for 2000*clk_uart_period;
    -------------------------
 
  end process;
  
  process
    begin
        wait for 10000*clk_uart_period;
        assert false
            report "[EXITO] >> Simulacion finalizada <<"
                severity failure;
    end process;

end test;