library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;
-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity PWM_Module is
    port(
		CLK			: in std_logic;
		RESET		: in std_logic;-- Active on low level
		DUTY		: in std_logic_vector(7 downto 0);
		ENABLE      : in std_logic;
		PWM_OUT		: out std_logic
		
		);
end entity;

architecture Behavioral of PWM_Module is
    
    signal COUNTER: unsigned(DUTY'range):="00000000";
    signal DUTY_IN: std_logic_vector(DUTY'range);
begin
    process(CLK, RESET)
    begin
		if RESET = '0' then
	   	   COUNTER <= (others=>'0');
		elsif rising_edge(CLK) then
	        if ENABLE = '0' then
                COUNTER <= (others => '0');
            else
                COUNTER <= COUNTER + 1;
            end if;
	    end if;
    end process;
    
    process(CLK)
    begin
        if (rising_edge(CLK)) then
            if (COUNTER="00000000") then
                DUTY_IN <= DUTY;
            end if;
        end if;
    end process;
    
    Compare: PWM_OUT <= '0' when (ENABLE = '0' or COUNTER > unsigned(DUTY_IN) or RESET='0') else  
    					'Z';
end Behavioral;