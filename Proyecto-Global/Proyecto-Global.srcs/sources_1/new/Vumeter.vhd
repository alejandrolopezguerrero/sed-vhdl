----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
use IEEE.std_logic_unsigned.all;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity VUMETER is
 generic (
    width : positive := 8;
    leds_string : positive := 16
    );
    Port(
    CLK: IN STD_LOGIC;
    RESET: IN STD_LOGIC;-- Active on low level
    BYTE_IN: IN STD_LOGIC_VECTOR(width-1 downto 0);
    LEDS_OUT: OUT STD_LOGIC_VECTOR(leds_string-1 downto 0)
    );
end VUMETER;

architecture Behavioral of VUMETER is

signal MAX: std_logic_vector(width-1 downto 0):=(others=>'0');
signal MIN: std_logic_vector(width-1 downto 0):=(others=>'0');
signal MAX_INTERVAL:std_logic_vector(width-1 downto 0):=(others=>'0');
signal BUFFER_OUT: std_logic_vector(leds_string-1 downto 0):=(others=>'0');
signal COUNTER: std_logic_vector(24 downto 0):=(others=>'0');

signal i :integer:=0;

begin
    
    
    
	Control: process(RESET,CLK)
    begin
        if RESET='0' then
            MAX_INTERVAL<=(others=>'0');
            BUFFER_OUT<=(others=>'0');
            COUNTER<=(others=>'0');
        elsif rising_edge(CLK) then
            COUNTER<=COUNTER+1; 
        end if;
    end process;
    
    process(COUNTER)
    begin
        if(COUNTER=(others=>'0'))then
            MAX<=(others=>'0');
            MIN<=(others=>'0');
        elsif (BYTE_IN>MAX) then
            MAX<=BYTE_IN;
        elsif (BYTE_IN<MIN) then
            MIN<=BYTE_IN;
        end if;
    end process;
    
    process (COUNTER)
    begin
        if(COUNTER=(others=>'1'))then
            MAX_INTERVAL<=(MAX-MIN);
        end if;
    end process;
    
    process (MAX_INTERVAL)
    begin
        for i in 1 to width-1 loop
            if(MAX_INTERVAL>std_logic_vector(to_unsigned(2**i, width))) then
                BUFFER_OUT(i)<='1';
                BUFFER_OUT(i-1)<='1';
            else
                BUFFER_OUT(i)<='0';
            end if;
        end loop;
    
    end process;
   
    LEDS_OUT<=BUFFER_OUT;

end Behavioral;

