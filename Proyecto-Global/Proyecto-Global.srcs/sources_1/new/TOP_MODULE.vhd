
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.numeric_std.all;
use IEEE.STD_LOGIC_UNSIGNED.ALL;
-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity TOP_MODULE is
  Port (
    CLK: IN STD_LOGIC; --Main Clock
    RESET: IN STD_LOGIC;-- Active on low level
    UART_IN: IN STD_LOGIC;-- UART Rx
    SOUND_PWM: OUT STD_LOGIC; --Output Sound
    SOUND_SD: OUT STD_LOGIC
   );
end TOP_MODULE;

architecture Structural of TOP_MODULE is
----------------------------------------
    component UART_RX

        port (
        i_Clk       : in  std_logic;
        i_RX_Serial : in  std_logic;
        o_RX_DV     : out std_logic;
        o_RX_Byte   : out std_logic_vector(7 downto 0)
        );
    end component;
----------------------------------------
    component PWM_Module 
        port(
		  CLK			: in std_logic;
		  RESET		: in std_logic;-- Active on low level
		  DUTY		: in std_logic_vector(7 downto 0);
		  ENABLE      : in std_logic;
		  PWM_OUT		: out std_logic
		  );
    end component;
----------------------------------------
    signal BUS_SOUND :STD_LOGIC_VECTOR(7 downto 0);
    signal clk_cntr_reg : std_logic_vector (1 downto 0) := (others=>'0');
begin

    process(CLK)
    begin
      if (rising_edge(CLK)) then
        clk_cntr_reg <= clk_cntr_reg + 1;
      end if;
    end process;
    
Communication: UART_RX port map(
        i_Clk=>clk_cntr_reg(1),
        i_RX_Serial=>UART_IN,
        o_RX_Byte=>BUS_SOUND
        );

PWM_Conv: PWM_Module port map(
        CLK=>clk_cntr_reg(0),
        RESET=>RESET,
		DUTY=>BUS_SOUND,
		ENABLE=>'1',
		PWM_OUT=>SOUND_PWM
    );
SOUND_SD<='1';
end Structural;
