-- Receptor USART sin paridad

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
use IEEE.std_logic_unsigned.all;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity USART is
    generic(
        width: positive :=8
    );
    Port(
    CLK_USART: IN STD_LOGIC;
    RESET: IN STD_LOGIC;-- Active on low level
    SIN: IN STD_LOGIC;
    POUT: OUT STD_LOGIC_VECTOR(width-1 downto 0)
    );
end USART;

architecture Behavioral of USART is

signal BUFFER_IN : std_logic_vector(width-1 downto 0);
signal BUFFER_OUT: std_logic_vector(width-1 downto 0);
signal Acumulator: std_logic_vector(3 downto 0):="0000";
signal ConvStrt: boolean :=FALSE; --Conversion Started
signal ConvCplt: boolean :=FALSE; --Conversion Completed

begin
    
    POUT<=BUFFER_OUT;
    
	Control: process(RESET,CLK_USART)
    begin
        if RESET='0' then
            BUFFER_IN<=(others=>'0');
            BUFFER_OUT<=(others=>'0');
            Acumulator<="0000"; 
            ConvStrt <= FALSE;
            ConvCplt <= FALSE;
 
         elsif rising_edge(CLK_USART) then
            if(BUFFER_IN="11111111" and SIN='0') then --Start bit detector
                ConvStrt <= TRUE;
                ConvCplt <= FALSE;
                Acumulator<= "0000";
            end if;
            BUFFER_IN<=BUFFER_IN(width-2 downto 0) & SIN;
            if ConvCplt then
                BUFFER_OUT<=BUFFER_IN;
                ConvCplt <= FALSE;
                ConvStrt <= FALSE;
            end if;
                   
            if (ConvStrt) then
                Acumulator<=(Acumulator+"0001");
                if Acumulator="0111" then 
                    ConvCplt <= TRUE;
                    ConvStrt <= FALSE;
                end if;
            end if;                
        end if;
    end process;

end Behavioral;