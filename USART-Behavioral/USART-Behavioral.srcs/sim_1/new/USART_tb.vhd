----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 03.02.2021 13:40:39
-- Design Name: 
-- Module Name: USART_tb - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity USART_tb is
end USART_tb;


architecture test of USART_tb is

COMPONENT USART
    PORT(
          CLK_USART: in STD_LOGIC;
          RESET: in STD_LOGIC;
          SIN: in STD_LOGIC;
          POUT: out STD_LOGIC_VECTOR(7 downto 0)
        ); 
    END COMPONENT;
    
   signal CLK : std_logic := '0';
   signal RESET : std_logic := '0';
   signal SIN : std_logic := '0';
   signal POUT: std_logic_vector(7 downto 0);
   
   constant clk_period : time := 10 ns;
begin
 uut: USART PORT MAP (
          CLK_USART=> CLK,
          RESET => RESET,
          SIN => SIN,
          POUT=>POUT
        );
CLK_TEST:process
  begin
  	CLK <= '0';
  	wait for clk_period/2;
  	CLK <= '1';
  	wait for clk_period/2;
  end process;
  
--RESET<= '1', '0' after 0.25*clk_period;
SIN_TEST:process
  begin
    RESET<= '0';
    wait for 0.25*clk_period;
    SIN<='1';
    RESET<= '1';
    wait for 8.75*clk_period;
    -------------------------
    SIN<='0';
    wait for 2*clk_period;
    SIN<='1';
    wait for 4*clk_period;
    SIN<='0';
    wait for 1*clk_period;
    SIN<='1';
    wait for 12*clk_period;
    -------------------------
    SIN<='0';
    wait for 1*clk_period;
    SIN<='1';
    wait for 1*clk_period;
    SIN<='0';
    wait for 4*clk_period;
    SIN<='1';
    wait for 12*clk_period;
    -------------------------
    RESET<= '0';
    wait for 0.25*clk_period;
    RESET<='1';
    SIN<='0';
    wait for 2*clk_period;
    SIN<='1';
    wait for 4*clk_period;
    SIN<='0';
    wait for 1*clk_period;
    SIN<='1';
    wait for 8*clk_period;
    
  end process;
  
  process
    begin
        wait for 50*clk_period;
        assert false
            report "[EXITO] >> Simulacion finalizada <<"
                severity failure;
    end process;

end test;
